/*******************************************************************************************
 *  TOP LEVEL MODULE 
 *          deserializer_io
 *
 *  AUTHOR: Antonin Zeman 
 *          https://gitlab.com/tondazeman10
 *  
 *  DATE:
 *          20/12/2022
 *
 *  MODULE DESCRIPTION:
 *          Deserialize logic that uses DDR for putting accepted bits together. This module
 *          contains DDR2 primitives for every LVDS channel. Output data is latched out 
 *          on system clock edge for synchronization. For porting to a different FPGA platform
 *          DDR primitives would have to be replaced.
 *
 ******************************************************************************************/   

/*******************************************************************************************
 * MODULE DECLARATION 
 ******************************************************************************************/    

module deserializer_io(
    input rst,
    input sys_clk,
    input dco_clk,
    input dco_clk_n,
    input frame,

    input data_a_0,
    input data_b_0,

    input data_a_1,
    input data_b_1,

    input data_a_2,
    input data_b_2,

    input data_a_3,
    input data_b_3,

    output reg [31:0] ser_out
    );

/*******************************************************************************************
 * SHIFT REG DECLARATIONS
 ******************************************************************************************/    

    reg [7:0] ser_frame;

    reg [11:0] ser_0_b;
    reg [11:0] ser_0_a;

    reg [11:0] ser_1_b;
    reg [11:0] ser_1_a;

    reg [11:0] ser_2_b;
    reg [11:0] ser_2_a;

    reg [11:0] ser_3_b;
    reg [11:0] ser_3_a;

/*******************************************************************************************
 * CLOCK BUFFERING 
 ******************************************************************************************/   

 // this step is required because different IO regions are used for different signals,
 // dco_clk_in can be used only in its IO bank.

   wire dco_clk_in;
     BUFG BUFG_dco (
      .O(dco_clk_in), 
      .I(dco_clk)     
   );
   wire dco_clk_in_n;
     BUFG BUFG_dco_n (
      .O(dco_clk_in_n), 
      .I(dco_clk_n)     
   );

/*******************************************************************************************
 * SIGNAL DECLARATIONS 
 ******************************************************************************************/   
wire frame_0, frame_1;
wire data_a_0_0, data_a_0_1;
wire data_b_0_0, data_b_0_1;

wire data_a_1_0, data_a_1_1;
wire data_b_1_0, data_b_1_1;

wire data_a_2_0, data_a_2_1;
wire data_b_2_0, data_b_2_1;

wire data_a_3_0, data_a_3_1;
wire data_b_3_0, data_b_3_1;

/*******************************************************************************************
 * IDDR2 PRIMITIVES DECLARATIONS 
 ******************************************************************************************/

      IDDR2 #(
      .DDR_ALIGNMENT("C0"), // Sets output alignment to "NONE", "C0" or "C1" 
      .INIT_Q0(1'b0), // Sets initial state of the Q0 output to 1'b0 or 1'b1
      .INIT_Q1(1'b0), // Sets initial state of the Q1 output to 1'b0 or 1'b1
      .SRTYPE("SYNC") // Specifies "SYNC" or "ASYNC" set/reset
      ) IDDR2_frame (
      .Q0(frame_0), // 1-bit output captured with C0 clock
      .Q1(frame_1), // 1-bit output captured with C1 clock
      .C0(dco_clk_in), // 1-bit clock input
      .C1(dco_clk_in_n), // 1-bit clock input
      .CE(1), // 1-bit clock enable input
      .D(frame),   // 1-bit DDR data input
      .R(0),   // 1-bit reset input
      .S(0)    // 1-bit set input
   );
   
    IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_a_0 (
      .Q0(data_a_0_0), 
      .Q1(data_a_0_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_a_0),   
      .R(0),   
      .S(0)    
   );

      IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_b_0 (
      .Q0(data_b_0_0), 
      .Q1(data_b_0_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_b_0),  
      .R(0),   
      .S(0)    
   );

   IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_a_1 (
      .Q0(data_a_1_0), 
      .Q1(data_a_1_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_a_1),  
      .R(0),   
      .S(0)    
   );

   IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_b_1 (
      .Q0(data_b_1_0), 
      .Q1(data_b_1_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_b_1),  
      .R(0),   
      .S(0)    
   );

   IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_a_2 (
      .Q0(data_a_2_0), 
      .Q1(data_a_2_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_a_2),  
      .R(0),   
      .S(0)    
   );

   IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_b_2 (
      .Q0(data_b_2_0), 
      .Q1(data_b_2_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_b_2),  
      .R(0),   
      .S(0)    
   );

      IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_a_3 (
      .Q0(data_a_3_0), 
      .Q1(data_a_3_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_a_3),  
      .R(0),   
      .S(0)    
   );

   IDDR2 #(
      .DDR_ALIGNMENT("C0"), 
      .INIT_Q0(1'b0), 
      .INIT_Q1(1'b0), 
      .SRTYPE("SYNC") 
   ) IDDR2_b_3 (
      .Q0(data_b_3_0), 
      .Q1(data_b_3_1), 
      .C0(dco_clk_in), 
      .C1(dco_clk_in_n), 
      .CE(1), 
      .D(data_b_3),  
      .R(0),   
      .S(0)    
   );


/*******************************************************************************************
 * SHIFT REGISTER LOGIC
 ******************************************************************************************/


// shift register putting bits in series in pairs 
   always @ (negedge dco_clk_in) begin 
        ser_frame[0] <= frame_0;
        ser_frame[1] <= frame_1;

        ser_frame[2] <= ser_frame[0];
        ser_frame[3] <= ser_frame[1];

        ser_frame[4] <= ser_frame[2];
        ser_frame[5] <= ser_frame[3];

        ser_frame[6] <= ser_frame[4];
        ser_frame[7] <= ser_frame[5];
   end 

   always @ (negedge dco_clk_in) begin 
        ser_0_a[6] <= ser_0_a[4];
        ser_0_a[7] <= ser_0_a[5];

        ser_0_a[4] <= ser_0_a[2];
        ser_0_a[5] <= ser_0_a[3];

        ser_0_a[2] <= ser_0_a[0];
        ser_0_a[3] <= ser_0_a[1];

        ser_0_a[0] <= data_a_0_0;
        ser_0_a[1] <= data_a_0_1;

   end 

   always @ (negedge dco_clk_in) begin 
        ser_0_b[6] <= ser_0_b[4];
        ser_0_b[7] <= ser_0_b[5];

        ser_0_b[4] <= ser_0_b[2];
        ser_0_b[5] <= ser_0_b[3];

        ser_0_b[2] <= ser_0_b[0];
        ser_0_b[3] <= ser_0_b[1];

        ser_0_b[0] <= data_b_0_0;
        ser_0_b[1] <= data_b_0_1;
   end 

   always @ (negedge dco_clk_in) begin 
        ser_1_a[6] <= ser_1_a[4];
        ser_1_a[7] <= ser_1_a[5];

        ser_1_a[4] <= ser_1_a[2];
        ser_1_a[5] <= ser_1_a[3];

        ser_1_a[2] <= ser_1_a[0];
        ser_1_a[3] <= ser_1_a[1];

        ser_1_a[0] <= data_a_1_0;
        ser_1_a[1] <= data_a_1_1;

   end 

   always @ (negedge dco_clk_in) begin
        ser_1_b[6] <= ser_1_b[4];
        ser_1_b[7] <= ser_1_b[5];

        ser_1_b[4] <= ser_1_b[2];
        ser_1_b[5] <= ser_1_b[3];

        ser_1_b[2] <= ser_1_b[0];
        ser_1_b[3] <= ser_1_b[1];

        ser_1_b[0] <= data_b_1_0;
        ser_1_b[1] <= data_b_1_1;
   end 

   always @ (negedge dco_clk_in) begin 
        ser_2_a[6] <= ser_2_a[4];
        ser_2_a[7] <= ser_2_a[5];

        ser_2_a[4] <= ser_2_a[2];
        ser_2_a[5] <= ser_2_a[3];

        ser_2_a[2] <= ser_2_a[0];
        ser_2_a[3] <= ser_2_a[1];

        ser_2_a[0] <= data_a_2_0;
        ser_2_a[1] <= data_a_2_1;

   end 

   always @ (negedge dco_clk_in) begin 
        ser_2_b[6] <= ser_2_b[4];
        ser_2_b[7] <= ser_2_b[5];

        ser_2_b[4] <= ser_2_b[2];
        ser_2_b[5] <= ser_2_b[3];

        ser_2_b[2] <= ser_2_b[0];
        ser_2_b[3] <= ser_2_b[1];

        // the connection on board is swapped therefore the negation
        // fixed in V1.0 sipm_readout_board 
        ser_2_b[0] <= data_b_2_0;
        ser_2_b[1] <= data_b_2_1;
   end

   always @ (negedge dco_clk_in) begin 
        ser_3_a[6] <= ser_3_a[4];
        ser_3_a[7] <= ser_3_a[5];

        ser_3_a[4] <= ser_3_a[2];
        ser_3_a[5] <= ser_3_a[3];

        ser_3_a[2] <= ser_3_a[0];
        ser_3_a[3] <= ser_3_a[1];

        ser_3_a[0] <= !data_a_3_0;
        ser_3_a[1] <= !data_a_3_1;

   end 

   always @ (negedge dco_clk_in) begin 
        ser_3_b[6] <= ser_3_b[4];
        ser_3_b[7] <= ser_3_b[5];

        ser_3_b[4] <= ser_3_b[2];
        ser_3_b[5] <= ser_3_b[3];

        ser_3_b[2] <= ser_3_b[0];
        ser_3_b[3] <= ser_3_b[1];

        ser_3_b[0] <= data_b_3_0;
        ser_3_b[1] <= data_b_3_1;
   end


/******************************************************************************
*  SHIFT REG BIT ORDER TABLE 
*     out_0 ...  b_1
*     out_1 ...  a_1
*     out_2 ...  b_2
*     out_3 ...  a_2
*     out_4 ...  b_3
*     out_5 ...  a_3
*     out_6 ...  b_4
*     out_7 ...  a_4
*
*******************************************************************************/

/*******************************************************************************************
 * SYNCHRONOUS LATCHING LOGIC
 ******************************************************************************************/

always @(negedge dco_clk_in) begin 
   // block for assigning bits whe frame is synchronous
   // with DCO, ie. on frame posedge there is dco posedge

   if (ser_frame == 8'b01110001) begin 

      // frame signal for debug 
 /*     
      ser_out[0] <= ser_frame[0];
      ser_out[1] <= ser_frame[1];
      ser_out[2] <= ser_frame[2];
      ser_out[3] <= ser_frame[3];
      ser_out[4] <= ser_frame[4];
      ser_out[5] <= ser_frame[5];
      ser_out[6] <= ser_frame[6];
      ser_out[7] <= ser_frame[7];
    */  

		ser_out[7] <= ser_0_a[6];
      ser_out[6] <= ser_0_b[6];
      ser_out[5] <= ser_0_a[5];
      ser_out[4] <= ser_0_b[5];
      ser_out[3] <= ser_0_a[4];
      ser_out[2] <= ser_0_b[4];
      ser_out[1] <= ser_0_a[3];
      ser_out[0] <= ser_0_b[3];

      ser_out[15] <= ser_1_a[6];
      ser_out[14] <= ser_1_b[6];
      ser_out[13] <= ser_1_a[5];
      ser_out[12] <= ser_1_b[5];
      ser_out[11] <= ser_1_a[4];
      ser_out[10] <= ser_1_b[4];
      ser_out[9] <= ser_1_a[3];
      ser_out[8] <= ser_1_b[3];

      ser_out[23] <= ser_2_a[6];
      ser_out[22] <= ser_2_b[6];
      ser_out[21] <= ser_2_a[5];
      ser_out[20] <= ser_2_b[5];
      ser_out[19] <= ser_2_a[4];
      ser_out[18] <= ser_2_b[4];
      ser_out[17] <= ser_2_a[3];
      ser_out[16] <= ser_2_b[3];

      ser_out[31] <= ser_3_a[6];
      ser_out[30] <= ser_3_b[6];
      ser_out[29] <= ser_3_a[5];
      ser_out[28] <= ser_3_b[5];
      ser_out[27] <= ser_3_a[4];
      ser_out[26] <= ser_3_b[4];
      ser_out[25] <= ser_3_a[4];
      ser_out[24] <= ser_3_b[4];
   
   // if frame signal shifts to be in sync with DCO falling edge
   // frame pattern changes and data bits have to be assigned also
   //shifted

   end else if (ser_frame == 8'b00111000) begin 
      /*
      ser_out[0] <= ser_frame[0];
      ser_out[1] <= ser_frame[1];
      ser_out[2] <= ser_frame[2];
      ser_out[3] <= ser_frame[3];
      ser_out[4] <= ser_frame[4];
      ser_out[5] <= ser_frame[5];
      ser_out[6] <= ser_frame[6];
      ser_out[7] <= ser_frame[7];
      */

      ser_out[0] <= ser_0_a[5];
      ser_out[1] <= ser_0_b[5];
      ser_out[2] <= ser_0_a[4];
      ser_out[3] <= ser_0_b[4];
      ser_out[4] <= ser_0_a[3];
      ser_out[5] <= ser_0_b[3];
      ser_out[6] <= ser_0_a[2];
      ser_out[7] <= ser_0_b[2];
		
		ser_out[15] <= ser_1_a[5];
      ser_out[14] <= ser_1_b[5];
      ser_out[13] <= ser_1_a[4];
      ser_out[12] <= ser_1_b[4];
      ser_out[11] <= ser_1_a[3];
      ser_out[10] <= ser_1_b[3];
      ser_out[9] <= ser_1_a[2];
      ser_out[8] <= ser_1_b[2];


      ser_out[23] <= ser_2_a[5];
      ser_out[22] <= ser_2_b[5];
      ser_out[21] <= ser_2_a[4];
      ser_out[20] <= ser_2_b[4];
      ser_out[19] <= ser_2_a[3];
      ser_out[18] <= ser_2_b[3];
      ser_out[17] <= ser_2_a[2];
      ser_out[16] <= ser_2_b[2];

      ser_out[31] <= ser_3_a[5];
      ser_out[30] <= ser_3_b[5];
      ser_out[29] <= ser_3_a[4];
      ser_out[28] <= ser_3_b[4];
      ser_out[27] <= ser_3_a[3];
      ser_out[26] <= ser_3_b[3];
      ser_out[25] <= ser_3_a[2];
      ser_out[24] <= ser_3_b[2];
      
   end
end 

endmodule
