/*******************************************************************************************
 *  TOP LEVEL MODULE 
 *          FURRy_TOP_level
 *
 *  AUTHOR: Antonin Zeman 
 *          https://gitlab.com/tondazeman10
 *  
 *
 *  DATE:
 *          20/12/2022
 *
 *  MODULE DESCRIPTION:
 *          Top level module containing that serves for interconnecting all other logic modules.         
 *          Contains also for IO buffer declaration. Each individual module is descrtibed in its
 *          .v file.
 *
 ******************************************************************************************/            
            
/*******************************************************************************************
 * MODULE DECLARATION 
 ******************************************************************************************/    

module FURRy_Top_Level  (
            


            // Global signals
            input       CLK20MHZ ,
            input       RESET_GLOBAL ,
            output      LED_ORNG ,

            output      UNUSED_KEEPER ,
            input       XCHIP_CLK_FROM_XCp ,
            input       XCHIP_CLK_FROM_XCn ,
        
            // FTDI related
            input       FT_CLK ,                                // ftdi comm
            input       FT_RXF_N ,                              // ftdi comm
            input       FT_TXE_N ,                              // ftdi comm
            
            output      FT_RD_N ,                               // ftdi comm
            output      FT_WR_N ,                               // ftdi comm
            output      FT_OE_N ,                               // ftdi comm
            inout       [3:0]FT_BE ,                            // ftdi comm
            inout       [31:0]FT_DATA ,                         // ftdi comm

            // Boot flash
            output      CONFIGMEM_CS ,
            output      CONFIGMEM_MOSI ,
            input       CONFIGMEM_MISO ,
            output      CONFIGMEM_SCK,
            output      XCHIP_DTAOUT,
            output      XCHIP_CLRB,

            // LPC inputs 
            // data clock out 
            input dco_p,
            input dco_n,

            input data_a_0_p,
            input data_a_0_n,
            input data_b_0_p,
            input data_b_0_n,

            input data_a_1_p,
            input data_a_1_n,
            input data_b_1_p,
            input data_b_1_n,

            input data_a_2_p,
            input data_a_2_n,
            input data_b_2_p,
            input data_b_2_n,

            input data_a_3_p,
            input data_a_3_n,
            input data_b_3_p,
            input data_b_3_n,

            input frame_n,
            input frame_p
    );

/*******************************************************************************************
 * LOCALPARAM DEFINES 
 ******************************************************************************************/    

    //  if USE_GLOBAL_SYSTEM_RESET==1 then RESET_GLOBAL input pin is connected to Spartan6
    //  global GSR signal (for details see: Spartan-6 FPGA Configuration User Guide, UG380, Chapter 4)
    localparam USE_GLOBAL_SYSTEM_RESET = 1;

/*******************************************************************************************
 * NET DECLARATIONS
 ******************************************************************************************/

    // deserializer sigs
    wire            dco;
    wire            dco_neg;
    wire            dco_in_p;
    wire            dco_in_n;
    wire            frame;
    wire [31:0]     deserialized_data;

    // FTDI transiever sigs 
    wire            FTDI_trans_en_to_pc;
    wire            FIFO_wr_to_pc;
    wire [31:0]     FTDI_data_to_pc;


    wire [31:0]     ftdi_data_from_pc;
    wire            ftdi_data_from_pc_empty;
    wire            ftdi_data_from_pc_rd;

    // global clocking and reset sigs 
    wire            Reset_Global_inner ;        
    // Connected directly to input pin; can be tied LO if USE_GLOBAL_SYSTEM_RESET==1
    // and all modules use correct reg initialization values in HDL code
    wire            Reset_Global_SysStartup ; 
    // Connected to GSR input of STARTUP global primitive;
    // when asserted, switches all regs to init state
    wire            Clk_100MHz_FTDI ;       
    // Direct connection to FT_CLK pin; 100MHz used as main clock for FTDI bus master
    // module, supposed also as main clk input 

/*******************************************************************************************
 * REG DECLARATIONS
 ******************************************************************************************/
    

/*******************************************************************************************
 * SIGNAL ASSIGNEMENTS
 ******************************************************************************************/

    assign  Reset_Global_inner      =   RESET_GLOBAL ;
    assign  Reset_Global_SysStartup =   RESET_GLOBAL ;




/*******************************************************************************************
 * CLOCKING PRIMITIVES DECLARATIONS 
 ******************************************************************************************/

    BUFG    Ints_FTDIClkBUFG(
        .I(FT_CLK),
        .O(Clk_100MHz_FTDI)
    ) ; 

/*******************************************************************************************
 * IO PRIMITIVES DECLARATIONS 
 ******************************************************************************************/
   
    IBUFGDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IOSTANDARD("LVDS_33") // Specify the input I/O standard
    ) IBUFGDS_dco (
        .O(dco), // Clock buffer output
        .I(dco_p), // Diff_p clock buffer input (connect directly to top-level port)
        .IB(dco_n) // Diff_n clock buffer input (connect directly to top-level port)
    );

    IBUFGDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFGDS_dco_n (
        .O(dco_neg), 
        .I(dco_n), 
        .IB(dco_p) 
    );

    IBUFGDS #(
      .DIFF_TERM("TRUE"),
      .IOSTANDARD("LVDS_33") 
    ) IBUFGDS_frame (
      .O(frame), 
      .I(frame_p),  
      .IB(frame_n) 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_a_0 (
        .O(data_a_0), 
        .I(data_a_0_p), 
        .IB(data_a_0_n) 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_b_0 (
        .O(data_b_0), 
        .I(data_b_0_p), 
        .IB(data_b_0_n) 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_a_1 (
        .O(data_a_1), 
        .I(data_a_1_p), 
        .IB(data_a_1_n) 
    );
    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_1b (
        .O(data_b_1), 
        .I(data_b_1_p), 
        .IB(data_b_1_n) 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_a_2 (
        .O(data_a_2), 
        .I(data_a_2_p), 
        .IB(data_a_2_n) 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_b_2 (
        .O(data_b_2), 
        .I(data_b_2_p), 
        .IB(data_b_2_n) 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_a_3 (
        .O(data_a_3), 
        .I(data_a_3_p), 
        .IB(data_a_3_n) 
    );
    IBUFDS #(
        .DIFF_TERM("TRUE"), 
        .IOSTANDARD("LVDS_33") 
    ) IBUFDS_data_b_3 (
        .O(data_b_3), 
        .I(data_b_3_p), 
        .IB(data_b_3_n) 
    );
   
/*******************************************************************************************
 * MODULE DECLARATIONS
 ******************************************************************************************/

    // transiever for data
    ftdi_transceiver_v02 ftdi_transceiver_v02_inst (
            .clk_ftdi_mod       (Clk_100MHz_FTDI),
            .FT_DATA            (FT_DATA),
            .FT_BE              (FT_BE),
            .FT_RXF_N           (FT_RXF_N),
            .FT_TXE_N           (FT_TXE_N),
            .FT_WR_N            (FT_WR_N),
            .FT_RD_N            (FT_RD_N),
            .FT_OE_N            (FT_OE_N),
            
            .sys_clk            (Clk_100MHz_FTDI),
            .rst                (Reset_Global_inner),

            .transmit_data      (FTDI_data_to_pc),
            .transmit_data_wr   (FIFO_wr_to_pc),
            .transmit_data_full (data_full_t),
            .transmit_enable    (FTDI_trans_en_to_pc),

            .receive_data       (ftdi_data_from_pc),
            .receive_data_rd    (ftdi_data_from_pc_rd),
            .receive_data_empty (ftdi_data_from_pc_empty),
            .receive_data_flush ()
        );

    // fifo communication controller
    fifo_controller fifo_controller_inst(
        .clk_1s         (clk_1s),
        .clk_100MHz     (Clk_100MHz_FTDI), 
        .rst            (RESET_GLOBAL),
    
        .FTDI_data      (FTDI_data_to_pc),
        .FIFO_wr        (FIFO_wr_to_pc),
        .FTDI_trans_en  (FTDI_trans_en_to_pc),
        .LED            (),
        
        .data_empty     (ftdi_data_from_pc_empty),
        .FIFO_rd        (ftdi_data_from_pc_rd),
        .FTDI_data_rec  (ftdi_data_from_pc),
        .FIFO_full      (data_full_t),
        .data_in        (deserialized_data)
        );

 
    deserializer_io deserializer_io_inst (
        .rst        (RESET_GLOBAL),
        .sys_clk    (Clk_100MHz_FTDI),
        .dco_clk    (dco),
        .dco_clk_n  (dco_neg),
        .frame      (frame),
        .data_a_0   (data_a_0),
        .data_b_0   (data_b_0),
        .data_a_1   (data_a_1),
        .data_b_1   (data_b_1),
        .data_a_2   (data_a_2),
        .data_b_2   (data_b_2),
        .data_a_3   (data_a_3),
        .data_b_3   (data_b_3),
        .ser_out    (deserialized_data)
        );
    
    
/*******************************************************************************************
 * STARTUP LOGIC  
 ******************************************************************************************/    


generate if ( USE_GLOBAL_SYSTEM_RESET==1 )
    // STARTUP_SPARTAN6: STARTUP Block (for details see: Spartan-6 FPGA Configuration User Guide, UG380, Chapter 4)
    // Spartan-6
    // Xilinx HDL Libraries Guide, version 14.5
    STARTUP_SPARTAN6    Inst_STARTUP_SPARTAN6 (
        .CFGCLK                 (),                             
        .CFGMCLK                (),                             
        .EOS                    (),                             
        .CLK                    (1'b0),                         
        .GSR                    (Reset_Global_SysStartup),      
        .GTS                    (1'b0),                         
        .KEYCLEARB              (1'b0)                          
    );
endgenerate


// Unused signals keepers
    wire    XChipClkOutDummyLoad_1 ;
    wire    XChipClkOutDummyLoad_2 ;
    reg     DummyLoadOutputSignal ;

    //IBUFDS  #( .DIFF_TERM("TRUE"), .IOSTANDARD("LVDS_33") )         Inst_ClkOut_Dummy_Load  ( .O(XChipClkOutDummyLoad_1), .I(XCHIP_CLK_FROM_XCp), .IB(XCHIP_CLK_FROM_XCn) );
    
    always  @(posedge CLK20MHZ) DummyLoadOutputSignal   <=  XChipClkOutDummyLoad_1 ;
    
    OBUF    #( .DRIVE(2), .IOSTANDARD("LVCMOS33"), .SLEW("SLOW") )  Inst_DummyLoadOutSignal ( .O(UNUSED_KEEPER), .I(DummyLoadOutputSignal)  );

/*******************************************************************************************
 * DEBUG  
 ******************************************************************************************/
    reg in;
    reg [31:0] led_cnt;
    assign LED_ORNG = in;  

    // includes bitslip logic, once run bitslip 
    always @(posedge dco)begin  
        if(RESET_GLOBAL) begin 
            led_cnt <= 0;  
        end 
        else if(led_cnt > 10000000) begin 
            in <= ~in;
            led_cnt <= 0;   
        end else begin
            led_cnt <= led_cnt + 1; 
        end   
            
    end 

endmodule
