/*******************************************************************************  
 *    MODULE: FTDI_tester
 *    
 *    DESCRIPTION:    
 *                    Module that sends data via FTDI every second. Every second
 *                    it sends an incremental value. Low frequency is chosen for
 *                    debug on PC side.
 *                    
 *                    Fuctions of module:
 *                      - if rec fifo not empty, read till it is 
 *                      - put lst 32 bits into reg that changes state
 *                    
 *                    
 *                    
 *    STATE_MACHINE:  STATE_IDLE  - no data sending, only listening
 *                  
 *                    STATE_BURST - send given amount of data
 *                                - after sending all data goto state idle
 *                                
 *                    STATE_CONT  - send data countinuosly
 *                                - exit this state only based on incoming message 
 * 
 * 
 *    NOTES:          Communication is controlled by PC app. FPGA can send data only when
 *                    it recieves request. Amount of data depends on length of 
 *                    trans_en pulse. 
 *                    
 *                    PC - read request
 *                    FPGA - trans en ege
 *                    FPGA - pushes out data as fast as possible
 *                    
 *                    
 *                    CONT MODE has to send request  peridilcally - non blocking exist
 *                    
 *                    FTDI clock works only when FTDI comm works
 *                    
 *                    
 *                    simple loopback
 *                      - read 4 bytes
 *                      - write back 
 *                      
 *                      
 *                      
 *                      
 *                      24.7. NOTES : 
 *                        - if I go with pulse width to 1 only one byte is read
 *                          also 3x byte values are the same 
 *                          
 *                          DONE 
 *                          
 *                          
 *                      25.7.
 *                        - trans works
 *                        - some extra documentation was made on how the timing should be 
 *                        - problem with reliable reading
 *                        - req is simple 4 byte loopback so maybe it is not a big deal 
 *                        - need to define comm delays and frame for loopback 
 *                        
 *                        DONE
 *                        
 *                        
 *                        TODO:
 *                          - with one try the transfer doesnt work
 *                          - it is hard to find out on which try it works since 
 *                            now both rd and trans enable is triggered
 *                              ---> data is always read from try 1 
 *                              
 *                          - need to implement tryes for wr and trans enable separately 
 *                          
 *                          -- extra note, delay of 1 Meg clk edges started working - 0.01 s delay 
 *                          
 *                          DONE 
 *                        
 *                        
 *                        7.8.
 *                          -- state machine: change state on incoming data
 *                            could be async same way reading data is done 
 *                            then cont data send has to be implemented 
 *
 *
 ****************************************************************************/
module FTDI_tester(
    input clk_1s,
    input clk_100MHz, 
    input rst,
    
    output [31:0] FTDI_data,
    output FIFO_wr,
    output reg FTDI_trans_en,
    output reg LED,
    
    input data_empty,
    output reg FIFO_rd,
    input [31:0] FTDI_data_rec,
    input FIFO_full,
    input [31:0] data_in
    );
  
  reg [31:0] recieved_data_reg;
  reg [7:0]  set_state_reg;
    
  reg [31:0] send_cnt;
  reg [31:0] read_cnt;
  reg [31:0] trans_cnt;
  reg [31:0] state_del_cnt;
  
  // ===================================================================
  // send cont sigs 
  reg       send_FIFO_f;
  reg [31:0] trans_en_cnt;
  
  reg FIFO_wr_s;
  wire FIFO_wr_drv;
  // there should be buffer, for now avoided by user constraint 
  assign FIFO_wr =    state == STATE_CONT ? (!ack_f ? FIFO_wr_drv : FIFO_wr_s) : FIFO_wr_s;
  assign FTDI_data =  state == STATE_CONT ?  !ack_f ? data_in :  (((recieved_data_reg & 32'h000000FF) << 8) | 32'h0000000A) :  (((recieved_data_reg & 32'h000000FF) << 8) | 32'h0000000A);
    
    
  reg trig1;
  reg trig2;
  
  always @(posedge clk_100MHz) begin 
    if(rst)  
      trig1 = 0;
    trig1 = ~trig1;
  end 
  always @(negedge clk_100MHz) begin 
    if(rst)  
      trig2 = 0;
    trig2 = ~trig2;
  end 
  // this doeasnt work, so sample rate is divided by 2 
  assign FIFO_wr_drv = (trig1 ^ trig2);
  

  
  
  // ===================================================================
  
  // state machine states declaration
  reg [31:0] state; 
  reg state_f;
  
  localparam STATE_IDLE     = 32'h00000001;
  localparam STATE_ACK      = 32'h00000002;
  localparam STATE_CONT     = 32'h00000003;
  
  
  localparam PULSE_WIDTH_TRANS_EN = 2000;
  localparam PULSE_WIDTH_FIFO_RD = 10;
  
  
  // read all data async 
  
  // if there is some data put the data into reg
  reg data_recieved_f;
  reg data_read_f;
  reg data_trans_f;
  reg state_del_f;
  reg ack_f;
  
  reg [31:0] pulse_cnt;
  reg [31:0] ack_response;
  
  // async read data 
  
  
  always @ (FTDI_data_rec) begin
    if(FTDI_data_rec)
      recieved_data_reg <= FTDI_data_rec;
  end 
  
  /*************************************************************************
   *  MAIN STATE MACHINE ASSIGNEMENT
  **************************************************************************/
  always @(posedge state_f or posedge rst) begin 
    if(rst) begin
      state <= STATE_IDLE;
    end 
    
    case(recieved_data_reg)
      STATE_IDLE: begin 
        state <= STATE_IDLE;
        LED <= 0;
      end 
      STATE_CONT: begin
        state <= STATE_CONT;
        LED <= 0;
      end 
      default: begin 
        state <= STATE_IDLE;
      end 
    endcase 
  end 
  
  /*************************************************************************
   *  LOGIC BASED ON MAIN STATE MACHINE 
  **************************************************************************/
  always  @(posedge clk_100MHz) begin 
    if(rst)begin 
      data_recieved_f <= 0;
      data_read_f     <= 0;
      read_cnt        <= 0;
      send_cnt        <= 0;
      trans_cnt       <= 0;
      FTDI_trans_en   <= 0;
      data_trans_f    <= 0;
      pulse_cnt       <= 0;
      state_f         <= 0;
      state_del_f     <= 0;
      state_del_cnt   <= 0;
      ack_f           <= 0;
      
      // data send block sigs 
      send_FIFO_f     <= 0;
      trans_en_cnt    <= 0;
      FIFO_wr_s       <= 0;
    end

  /*************************************************************************
   *  STATE IDLE 
  **************************************************************************/
    
    if(state == STATE_IDLE) begin 
      // read flag assignement 
      // reads data at all times 
        if((!data_empty) && (data_recieved_f == 0)) begin    
          data_recieved_f <= 1;
          ack_f <= 1;
          read_cnt <= 0;
          trans_cnt <= 0;
          send_cnt <= 0;
        end
        
          if(data_recieved_f == 1)
            read_cnt <= read_cnt + 1;
          if(data_read_f == 1)
            send_cnt <= send_cnt + 1;
          if(data_trans_f == 1)
            trans_cnt <= trans_cnt +1;
          if(state_del_f == 1)
            state_del_cnt <= state_del_cnt +1;
          
          pulse_cnt <= pulse_cnt +1;
             
          // after read delay reading data 
          if (data_recieved_f == 1 && read_cnt > 300) begin 
            FIFO_rd <= 1; 
            data_recieved_f <= 0;
            
            pulse_cnt <= 0;
          end else if ((data_recieved_f == 0) && (read_cnt > 300) && (pulse_cnt >= PULSE_WIDTH_FIFO_RD)) begin 
            FIFO_rd <= 0;
            data_read_f <= 1;
            read_cnt <= 0;
          end 
          
          
          // after reading data send data 
          if((data_read_f == 1) && (send_cnt > 300)) begin 
            FIFO_wr_s <= 1;
            data_read_f <= 0;
            pulse_cnt <= 0;
          end else if((data_read_f == 0) && (send_cnt > 300)) begin 
            data_trans_f <= 1;
            FIFO_wr_s <= 0;
            send_cnt <= 0;
          end 
          
      
          // after writing data send data 
          if((data_trans_f == 1) && (trans_cnt > 1000000)) begin 
            FTDI_trans_en <= 1; 
            data_trans_f <= 0;
            pulse_cnt <= 0;
            
          end else if((data_trans_f == 0) && (trans_cnt > 1000000) && (pulse_cnt >= PULSE_WIDTH_TRANS_EN)) begin 
            // end of whole send sequence, null all important flags and vars 
            FTDI_trans_en <= 0;
            trans_cnt <= 0;
            send_cnt <= 0;
            
            
            // a 3 clk edges delay to be added here so ACK MESSAGE would have enough time to be send 
            state_del_f <= 1;
          end 
          
          if((state_del_f == 1) && (state_del_cnt > 100000)) begin 
            state_f <= 1;
            state_del_f <= 0;
            // this triggers state change 
            // go to different state ack routine to turn of state_f
            
            
          end else if((state_del_f == 0) && (state_del_cnt > 100000))  begin 
            state_f <= 0;
            ack_f <= 0;
            state_del_cnt <= 0;
          end 
    end // state idle 
    
  /*************************************************************************
   *  STATE CONT 
  **************************************************************************/
    if(state == STATE_CONT) begin 
      // read flag assignement 
      // reads data at all times 
      if((!data_empty) && (data_recieved_f == 0)) begin    
        data_recieved_f <= 1;
        ack_f <= 1;
          
        read_cnt <= 0;
        trans_cnt <= 0;
        send_cnt <= 0;
      end
        
      if(data_recieved_f == 1)
        read_cnt <= read_cnt + 1;
      if(data_read_f == 1)
        send_cnt <= send_cnt + 1;
      if(data_trans_f == 1)
        trans_cnt <= trans_cnt +1;
      if(state_del_f == 1)
        state_del_cnt <= state_del_cnt +1;
          
      pulse_cnt <= pulse_cnt +1;

      // after read delay reading data 
      if (data_recieved_f == 1 && read_cnt > 300) begin 
        FIFO_rd <= 1; 
        data_recieved_f <= 0;       
        pulse_cnt <= 0;
      end else if ((data_recieved_f == 0) && (read_cnt > 300) && (pulse_cnt >= PULSE_WIDTH_FIFO_RD)) begin 
        FIFO_rd <= 0;
        data_read_f <= 1;
        read_cnt <= 0;
      end 
          
          
      // after reading data send data 
      if((data_read_f == 1) && (send_cnt > 300)) begin 
        FIFO_wr_s <= 1;
        data_read_f <= 0;
        pulse_cnt <= 0;
      end else if((data_read_f == 0) && (send_cnt > 300)) begin 
        data_trans_f <= 1;
        FIFO_wr_s <= 0;
        send_cnt <= 0;
      end 
          
      
      // after writing data send data 
      if((data_trans_f == 1) && (trans_cnt > 1000000)) begin 
        FTDI_trans_en <= 1; 
        data_trans_f <= 0;
        pulse_cnt <= 0;
            
      end else if((data_trans_f == 0) && (trans_cnt > 1000000) && (pulse_cnt >= PULSE_WIDTH_TRANS_EN)) begin 
        // end of whole send sequence, null all important flags and vars 
        FTDI_trans_en <= 0;
        trans_cnt <= 0;
        send_cnt <= 0;
            
            
        // a 3 clk edges delay to be added here so ACK MESSAGE would have enough time to be send 
        state_del_f <= 1;
      end 
          
      if((state_del_f == 1) && (state_del_cnt > 100000)) begin 
        state_f <= 1;
        state_del_f <= 0;
        // this triggers state change 
        // go to different state ack routine to turn of state_f
            
            
      end else if((state_del_f == 0) && (state_del_cnt > 100000))  begin 
        state_f <= 0;
        ack_f <= 0;
        state_del_cnt <= 0;
      end


    if(ack_f == 0) begin 
      
      
      if(FIFO_full) begin 
        send_FIFO_f <= 1;
        FTDI_trans_en <= 1;
      end
      
      if(send_FIFO_f) begin
        trans_en_cnt <= trans_en_cnt + 1;
        FTDI_trans_en <= 0;
        
      end 
      
      if(trans_en_cnt > 2000)
        send_FIFO_f <= 0;
    end 
  
    end // state idle 
    

    
    
  end // always clk

endmodule