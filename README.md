# master_thesis
This directory contains master thesis that I completed in the beginning of the year 2023 at the Czech technical university.

## Other thesis repositories

Following repositories are also part of master thesis development: 
<br /><br />
Kicad project containing SiPM readout board PCB development:<br />
    https://gitlab.com/tondazeman10/sipm<br />
Simple .c script running on ATTINY acting as a data loader for SiPM readout board ADC registers:<br />
    https://gitlab.com/tondazeman10/ltc2175_programmer<br />
CPP application that reads out SiPM DATA from USB interface and runs a matlab script to display said data:<br />
    https://gitlab.com/tondazeman10/ftdi_read<br />
Project containing rudamentary HDL code for simulating operation of LTC2175 digital interface:<br />
    https://gitlab.com/tondazeman10/ltc2175<br />
